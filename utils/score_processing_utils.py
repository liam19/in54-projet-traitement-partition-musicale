import numpy as np
import cv2 as cv
from collections import Counter
import matplotlib.pyplot as plt
from models.rectangle import Rectangle
from models.music_stave import MusicStave
from models.symbol_type import SymbolType


def do_staff_removal(img, line_height):
    """Remove staff lines from a score image.

    Args:
        img ([type]): image to process
        line_height ([int]): staff line height

    Returns:
        [type]: processed image
    """
    # Extract lines
    kernel_line = cv.getStructuringElement(
        cv.MORPH_RECT, (1, int(line_height / 5)))
    clean_lines = cv.erode(img, kernel_line, iterations=1)
    clean_lines = cv.dilate(clean_lines, kernel_line, iterations=1)

    staff_lines_pos = clean_lines

    cv.imwrite("OUT/no_lines.png", cv.bitwise_not(staff_lines_pos))

    return staff_lines_pos


def get_only_lines(img, line_height):
    """Remove staff lines from a score image.

    Args:
        img ([type]): image to process
        line_height ([int]): staff line height

    Returns:
        [type]: processed image
    """
    # Extract lines
    kernel_line = cv.getStructuringElement(
        cv.MORPH_RECT, (1, int(line_height / 5)))
    clean_lines = cv.erode(img, kernel_line, iterations=1)
    clean_lines = cv.dilate(clean_lines, kernel_line, iterations=1)

    staff_lines_pos = img - clean_lines
    cv.imwrite('OUT/only_lines.png', staff_lines_pos)

    return staff_lines_pos


def get_h_proj(img):
    """Computes the horizontal projection by summing values on each row of `img`

    Args:
        img([type]): image to process

    Returns:
        [np.array]: the projection
    """
    return np.sum(img, 1)


def get_v_proj(img):
    """Computes the vertical projection by summing values on each column of `img`

    Args:
        img([type]): image to process

    Returns:
        [np.array]: the projection
    """
    return np.sum(img, 0)


def count_horizontal_lines(img, min_gap=1, min_height=1, threshold=1):
    proj = get_h_proj(img)
    m = np.max(proj)
    candidates = np.argwhere(proj > (m * threshold / 100))

    if len(candidates) < 2:
        return 0

    nb_h = 0
    last = candidates[0][0]
    height = 1

    for c in candidates[1:]:
        c = c[0]
        g = c - last
        if g < min_gap:
            height += g
        else:
            nb_h += 1 if height >= min_height else 0
            height = 1
        last = c

    if height >= min_height:
        nb_h += 1

    return nb_h


def get_horizontal_lines_pos(img, threshold = -1):
    """Return array with staff lines position.

    Args:
        img (OpenCV binarized image): image of the sheet music WITH ONLY lines (apply get_only_lines fonction)
        threshold (int, optional): Defaults to size of the image/2.

    Returns:
        list: list with lines positions 
    """
    if threshold == -1:
        threshold = img.shape[1] / 2
    proj = get_h_proj(img)/255
    
    cd = np.argwhere(proj > threshold)
    staff_lines_pos = []
    for i in range(0, len(cd) - 1):
        if cd[i][0] + 1 != cd[i + 1][0]:
            staff_lines_pos.append(cd[i][0])
    staff_lines_pos.append(cd[len(cd) - 1][0])
    print(len(staff_lines_pos))
    return staff_lines_pos


def get_middle_lines_space(lines_pos):
    middle_pos = []
    for i in range(4, len(lines_pos)-1, 5):
        middle_pos.append((lines_pos[i] + lines_pos[i + 1]) / 2)
    return middle_pos


def get_reference_lengths(img, threshold=70):
    proj = get_h_proj(img)
    m = np.max(proj)

    thresh = m * threshold / 100
    candidates = np.argwhere(proj > thresh)

    if len(candidates) < 5:
        return None

    last = candidates[0]
    staff_thicknesses = {}
    staff_heights = {}

    count = 1

    for c in candidates[1:]:
        step = int(c - last)
        if step > 1:
            n = staff_heights.get(step, 0)
            staff_heights[step] = n + 1

            n = staff_thicknesses.get(count, 0)
            staff_thicknesses[count] = n + 1

            count = 1
        else:
            count += 1

        last = c

    most_common_thickness = Counter(staff_thicknesses).most_common(n=1)[0][0]
    most_common_heights = Counter(staff_heights).most_common(n=1)[0][0]

    return most_common_thickness, most_common_heights


def get_staff_lines_info(img):
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    thresh = cv.threshold(
        gray, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]
    staff_thickness, staff_height = get_reference_lengths(thresh)
    return staff_thickness, staff_height


def remove_duplicates(coords: list, threshold: float=10):
    """Removes similar tuples from coords.

    Args:
        coords (list): list of tuples to clean
        threshold (int, optional): Defaults to 10.

    Returns:
        list: cleaned list of tuples 
    """
    if len(coords) == 0:
        return coords

    final_coords = [coords[0]]

    for coord in coords[1:]:
        found = False
        for final in final_coords:
            if is_similar(coord, final, threshold):
                found = True
                break

        if not found:
            final_coords.append(coord)

    return final_coords


def is_similar(coord: tuple, curr_coord: tuple, threshold):
    up = (curr_coord[0] + threshold, curr_coord[1] +
          threshold)  # upper bound of tuple curr_coord
    low = (curr_coord[0] - threshold, curr_coord[1] -
           threshold)  # lower bound of tuple curr_coord

    cond1 = coord[0] <= up[0]
    cond2 = coord[1] <= up[1]
    cond3 = coord[0] >= low[0]
    cond4 = coord[1] >= low[1]

    if cond1 and cond2 and cond3 and cond4:
        return True

    return False


def is_sim(candidat, curr, threshold):
    if curr + threshold >= candidat >= curr - threshold:
        return True

    return False


def compute_music_staves(img):
    img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    img_bin = cv.threshold(img_gray, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]

    img_only_lines = get_only_lines(img_bin, 22)
    staff_lines_pos = get_horizontal_lines_pos(img_only_lines, 1000)
    inter_line = get_middle_lines_space(staff_lines_pos)

    music_staves = []
    pos = []
    threshold = 60

    i = 0
    for r in staff_lines_pos:
        # 5 staff lines = 1 stave
        
        if i % 5 == 0 and i > 0:
            # create Music Stave
            bounding_rec = Rectangle(0, pos[0] - threshold, img.shape[1], (pos[4] - pos[0]) + 2 * threshold)
            music_staves.append(MusicStave(bounding_rec, pos))
            # reset count
            i = 0
            pos = []
        i += 1
        pos.append(r)

    # add last staff
    bounding_rec = Rectangle(0, pos[0] - threshold, img.shape[1], (pos[4] - pos[0]) + 2 * threshold)
    music_staves.append(MusicStave(bounding_rec, pos))

    return music_staves


def compute_music_staves2(img):
    img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    img_bin = cv.threshold(img_gray, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]

    img_only_lines = get_only_lines(img_bin, 22)
    staff_lines_pos = get_horizontal_lines_pos(img_only_lines, 1000)
    inter_line = get_middle_lines_space(staff_lines_pos)
    inter_line.insert(0, 0)
    inter_line.append(img.shape[0])

    music_staves = []
    threshold = 60

    for i in range(0, len(inter_line)-1):
        bounding_rec = Rectangle(0, inter_line[i], img.shape[1], inter_line[i + 1] - inter_line[i])
        music_staves.append(MusicStave(bounding_rec, staff_lines_pos[i*5:(i*5+4)+1]))

    return music_staves


def sort_structure_list(in_list, staves):
    out_list = []
    for i in range(0, len(staves)):
        temp = list(filter(lambda struc: struc[3] == i, in_list))
        temp.sort(key = lambda struc: struc[1])
        out_list += temp
    return out_list


def note_to_str(note, staves):
    do_code = None
    octave_offset = None
    res = ""
    
    x, y = note.position.get_gravity_center()
    for stave in staves:
        if stave.contains(x, y):
            if stave.key == SymbolType.CLE_SOL:
                do_code = 5
                octave_offset = 4
            elif stave.key == SymbolType.CLE_FA:
                do_code = 3
                octave_offset = 3

            pitch = note.pitch.step
            note.pitch.compute_octave(stave.key)
            octave = note.pitch.octave + octave_offset
            if pitch == do_code:
                res = res + "C" + str(octave)
            elif pitch == (do_code + 1) % 7:
                res = res + "D" + str(octave)
            elif pitch == (do_code + 2) % 7:
                res = res + "E" + str(octave)
            elif pitch == (do_code + 3) % 7:
                res = res + "F" + str(octave)
            elif pitch == (do_code + 4) % 7:
                res = res + "G" + str(octave)
            elif pitch == (do_code + 5) % 7:
                res = res + "A" + str(octave)
            elif pitch == (do_code + 6) % 7:
                res = res + "B" + str(octave)
    return res


def resolve_duplicates(music_object_structure: list, threshold=0.8):
    if len(music_object_structure) == 0:
        return music_object_structure

    resolved_len = 1
    resolved = [None] * len(music_object_structure)

    resolved[0] = music_object_structure[0]
    last_area = resolved[resolved_len - 1][4].position.area()

    for obj in music_object_structure[1:]:
        current_area = obj[4].position.area()
        intersecting_area = resolved[resolved_len - 1][4].position.intersecting_area(obj[4].position)
        total_area = last_area + current_area - intersecting_area
        c = intersecting_area / total_area

        if c <= threshold:
            resolved[resolved_len] = obj
            last_area = current_area
            resolved_len += 1

    return resolved[:resolved_len]


# print(len(staff_lines_pos))
# print("---------------------------------------")
# res_middle = get_middle_lines_space(staff_lines_pos)
# for r in res_middle:
#     print(r)
# print(len(res_middle))
# h_proj = get_h_proj(img) / 255

# plt.figure()
# plt.plot(h_proj)
# plt.show()
