import tkinter as tk
import tkinter.filedialog as tkfd
from PIL import Image, ImageTk
from processing.processing import process


class GUI:
    def __init__(self, root):
        self.CANVAS_WIDTH = 800
        self.CANVAS_HEIGHT = 600
        self.root = root
        root.title("Lecteur de partition")
        root.geometry("800x800")

        self.canvas = tk.Canvas(root, width=800, height=600, bg="white")
        self.buttonOpen = tk.Button(
            root, text="Ouvrir partition", command=self.openImage)
        self.buttonRead = tk.Button(
            root, text="Lire", command=self.processImage)


        self.tempoFrame = tk.Frame(self.root)
        self.label = tk.Label(self.tempoFrame, text = "Tempo : ")
        self.tempo_entry = tk.Entry(self.tempoFrame)
        self.tempo_entry.insert(0, "80")

        self.currentStepVar = tk.StringVar()
        self.currentStepLabel = tk.Label(root, textvariable = self.currentStepVar)

        self.canvas.grid(row=0, column=0)
        self.buttonOpen.grid(row=1, column=0, pady=10)
        self.tempoFrame.grid(row = 2, column = 0, pady = 10)
        self.tempo_entry.grid(row = 0, column = 1, padx = 10)
        self.label.grid(row = 0, column = 0)
        self.buttonRead.grid(row=3, column=0, pady=10)
        self.currentStepLabel.grid(row = 4, column = 0, pady = 10)

    def openImage(self, filepath=None):
        if filepath == None:
            self.filepath = tkfd.askopenfilename(title="Ouvrir partition...", filetypes=(
                ("PNG files", "*.png"), ("JPEG files", "*.jpg"), ("GIF files", "*.gif"), ("All files", "*.*")))      
        else:
            self.filepath = filepath

        image = Image.open(self.filepath)
        width, height = image.size
        if width > self.CANVAS_WIDTH or height > self.CANVAS_HEIGHT:  # adapt image size to window
            if width >= height:
                basewidth = self.CANVAS_WIDTH
                wpercent = (basewidth / float(width))
                hsize = int((float(height) * float(wpercent)))
                image = image.resize((basewidth, hsize), Image.ANTIALIAS)
            else:
                baseheight = self.CANVAS_HEIGHT
                hpercent = (baseheight / float(height))
                wsize = int(float(width) * float(hpercent))
                image = image.resize((wsize, baseheight), Image.ANTIALIAS)

        self.displayImage = ImageTk.PhotoImage(image)
        self.canvas.create_image(
            self.CANVAS_WIDTH/2, self.CANVAS_HEIGHT/2, image=self.displayImage)

    def processImage(self):
        new_filepath = process(self.filepath, int(self.tempo_entry.get()), self.currentStepVar, self.root)
        self.openImage(new_filepath)
        
