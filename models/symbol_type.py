from enum import Enum, auto, unique

@unique
class SymbolType(Enum):
    SILENCE2 = auto()
    SILENCE4 = auto()
    CLE_SOL = auto()
    CLE_FA = auto()
    DIESE = auto()
    BECARRE = auto()
    BEMOL = auto()
    DEMI_SILENCE = auto()
    QUART_SILENCE = auto()
    SILENCE = auto()
    STRUCTURE = auto()
    POINT = auto()
    MESURE = auto()
