# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 09:36:52 2020

@author: liam1
"""

from models.note_type import NoteType
from models.pitch import Pitch
from models.rectangle import Rectangle
from models.music_object import MusicObject


class Note(MusicObject):
    
    def __init__(self, pitch, duration, note_type, position:Rectangle, quavers=0):
        MusicObject.__init__(self, position)
        self.pitch = pitch
        self.note_type = note_type
        self.quavers = quavers
        self.str_code = None
        self.duration = 0

    def compute_duration(self):
        if self.note_type == NoteType.BLACK:

            self.duration = 1 / pow(2, self.quavers)
        elif self.note_type == NoteType.WHITE:
            self.duration = 2
        else:
            self.duration = 4
            
    def apply_point(self):
        self.duration = self.duration + self.duration / 2

    def set_str_code(self, str_code):
        self.str_code = str_code

    def set_natural(self):
        self.str_code = self.str_code.replace('#', '')
        self.str_code = self.str_code.replace('-', '')
        
    def set_flat(self):
        self.set_natural()
        self.str_code = self.str_code[:1] + '-' + self.str_code[1:]

    def set_sharp(self):
        self.set_natural()
        self.str_code = self.str_code[:1] + '#' + self.str_code[1:]
