# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 09:37:40 2020

@author: liam1
"""
from models.symbol_type import SymbolType

class Pitch:
    def __init__(self, global_note_pitch):
        self.step = abs(global_note_pitch % 7)
        self.octave = -1
        self.global_note_pitch = global_note_pitch

    def compute_octave(self, key):
        if key == SymbolType.CLE_SOL:
            self.octave = (self.global_note_pitch + 2) // 7
        elif key == SymbolType.CLE_FA:
            self.octave = (self.global_note_pitch + 4) // 7
            
