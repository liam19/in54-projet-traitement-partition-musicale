class Rectangle:
    """
    Describes a simple rectangle by its upper left corner, width and height.
    The origin of the coordinate systems can also be given
    """

    def __init__(self, x, y, w, h, x_o=0, y_o=0):
        """Creates an instance of `Rectangle`

        Args:
            x([int]): x coordinate of the rectangle upper left corner
            y([int]): y coordinate of the rectangle upper left corner
            w([int]): rectangle width
            h([int]): rectangle height
            x_o([int], optional): x coordinate of the origin
            y_o([int], optional): y coordinate of the origin
        """
        self.x_o = int(x_o)
        self.y_o = int(y_o)
        self.x = int(x)
        self.y = int(y)
        self.h = int(h)
        self.w = int(w)

    def create_origin(self):
        """Creates a new instance of `self` where the origin has been moved to the left corner of the rectangle

        Args:

        Returns:
            [Rectangle]: the new rectangle
        """
        return Rectangle(0, 0, self.w, self.h, self.x_o + self.x, self.y_o + self.y)

    def get_gravity_center(self):
        x1, y1 = self.to_start_point()
        x2, y2 = self.to_end_point()
        return (x1 + x2) / 2, (y1 + y2) / 2

    def crop_img(self, img):
        """Crops `img` to the rectangle defined by `self`

        Args:
            img([type]): the original image
        Returns:
            [type]: the cropped image
        """

        y = self.y_o + self.y
        x = self.x_o + self.x
        return img[y:y + self.h, x:x + self.w]

    def to_start_point(self):
        """Gives the rectangle upper left corner

        Args:

        Returns:
            [int, int]: the upper left corner of the rectangle
        """
        return self.x_o + self.x, self.y_o + self.y

    def to_end_point(self):
        """Gives the rectangle bottom right corner

        Args:

        Returns:
            [int, int]: the bottom right corner of the rectangle
        """
        return self.x_o + self.x + self.w, self.y_o + self.y + self.h

    def contains_point(self, x, y):
        x1, y1 = self.to_start_point()
        x2, y2 = self.to_end_point()

        return x1 <= x <= x2 and y1 <= y <= y2

    def intersecting_area(self, o):
        x1, y1 = self.to_start_point()
        x2, y2 = self.to_end_point()

        x3, y3 = o.to_start_point()
        x4, y4 = o.to_end_point()

        x = min(x2, x4) - max(x1, x3)
        y = min(y2, y4) - max(y1, y3)

        if x < 0 or y < 0:
            return 0

        return x * y

    def area(self):
        x1, y1 = self.to_start_point()
        x2, y2 = self.to_end_point()

        return (x2 - x1) * (y2 - y1)

    def horizontal_enlarge(self, margin, max_width):
        enlarged = self.__copy__()
        enlarged.x -= margin
        enlarged.w += 2 * margin

        if enlarged.x < 0:
            enlarged.x_o += enlarged.x
            enlarged.x = 0
            enlarged.x_o = enlarged.x_o if enlarged.x_o > 0 else 0

        if enlarged.w > max_width:
            enlarged.w = max_width

        return enlarged

    def vertical_enlarge(self, margin, max_height):
        enlarged = self.__copy__()
        enlarged.y -= margin
        enlarged.h += 2 * margin

        if enlarged.y < 0:
            enlarged.y_o += enlarged.x
            enlarged.y = 0
            enlarged.y_o = enlarged.y_o if enlarged.y_o > 0 else 0

        if enlarged.w > max_height:
            enlarged.w = max_height

        return enlarged

    def corner_to_corner_mean(self, r2):
        x11, y11 = self.to_start_point()
        x12, y12 = self.to_end_point()

        x21, y21 = r2.to_start_point()
        x22, y22 = r2.to_end_point()

        return 0.25 * (
            self.__dist(x11, x21, y11, y21) +
            self.__dist(x12, x22, y11, y21) +
            self.__dist(x11, x21, y12, y22) +
            self.__dist(x12, x22, y12, y22)
        )

    @staticmethod
    def __dist(x1, x2, y1, y2):
        return abs(x1 - x2) + abs(y1 - y2)

    @staticmethod
    def from_two_rects(r1, r2):
        s1 = r1.to_start_point()
        s2 = r2.to_start_point()
        e1 = r1.to_end_point()
        e2 = r2.to_end_point()

        s = (min(s1[0], s2[0]), min(s1[1], s2[1]))
        e = (max(e1[0], e2[0]), max(e1[1], e2[1]))

        return Rectangle(s[0], s[1], e[0] - s[0], e[1] - s[1])

    def __copy__(self):
        return Rectangle(self.x, self.y, self.w, self.h, self.x_o, self.y_o)
