from models.rectangle import Rectangle
from models.music_object import MusicObject
from models.symbol_type import SymbolType


class Symbol(MusicObject):

    def __init__(self, symbol_type: SymbolType, position: Rectangle):
        MusicObject.__init__(self, position)
        self.symbol_type = symbol_type
        self.pitch = 0

    def set_pitch(self, pitch):
        self.pitch = pitch
