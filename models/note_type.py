# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 12:20:51 2020

@author: liam1
"""

from enum import Enum, unique, auto


@unique
class NoteType(Enum):
    BLACK = auto()
    WHITE = auto()
    WHOLE = auto()
