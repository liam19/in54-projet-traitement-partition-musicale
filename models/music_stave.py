from models.rectangle import Rectangle


class MusicStave:
    def __init__(self, bounding_rec:Rectangle, staff_lines_pos: list):
        self.bounding_rec = bounding_rec
        self.staff_lines_pos = staff_lines_pos  # 5 staff lines
        self.key = None

    def contains(self, x, y):
        return self.bounding_rec.contains_point(x, y)

    def set_key(self, key):
        print(key)
        self.key = key
