from gui.gui import GUI
from tkinter import Tk
from processing.processing import process

if __name__ == "__main__":
    root = Tk()
    gui = GUI(root)
    root.mainloop()
