import cv2 as cv
import os
from omrdatasettools import Downloader, OmrDataset
from processing.note_detector import NoteDetector
from processing.symbol_recognition import SymbolRecognition
from utils.score_processing_utils import get_staff_lines_info, compute_music_staves2, sort_structure_list, \
    resolve_duplicates
from models.note_type import NoteType
from processing.music_xml_creator import MusicXMLCreator

DATASET_FOLDER = '../dataset'
DATASET = OmrDataset.DeepScores_V1_Extended
OUT_FOLDER = "OUT/"
DATASET_FOLDER_NAMES = {
    'DeepScores_V1_Extended': 'deep_scores_dense_extended',
}


def download_dataset(force=False):
    """Download the dataset `DATASET` in `DATASET_FOLDER` folder.
    """

    if not os.path.exists(OUT_FOLDER):
        os.mkdir(OUT_FOLDER)
        
    if force or not os.path.exists(DATASET_FOLDER):
        downloader = Downloader()
        downloader.download_and_extract_dataset(DATASET, DATASET_FOLDER)
        os.remove(DATASET.get_dataset_filename())

    


def process(img_path, tempo_mid, current_step, root):
    result_path = 'OUT/out.jpg'

    current_step.set("Ouverture de l'image")
    root.update()

    img = cv.imread(img_path)
    img = cv.resize(img, (2707, 3828), interpolation=cv.INTER_AREA)

    current_step.set("Reconnaissance des portées")
    root.update()
    staff_thickness, staff_height = get_staff_lines_info(img)
    # Compute music staves and related staff lines
    music_staves = compute_music_staves2(img)

    # Symbol detection
    current_step.set("Détection des symboles")
    root.update()
    sr = SymbolRecognition(img, staff_height, music_staves)

    # Note detection
    current_step.set("Détection des notes")
    root.update()
    detector = NoteDetector(staff_thickness, staff_height, sr.symbols, music_staves, threshold=0.8)
    notes = detector.find_notes(img)

    current_step.set("Lecture des éléments détectés")
    root.update()
    symbols_structure = SymbolRecognition.get_symbols_structure(sr, music_staves)
    notes_structure = NoteDetector.get_notes_structure(notes, music_staves)
    music_object_structure = sort_structure_list(symbols_structure + notes_structure, music_staves)
    music_object_structure = resolve_duplicates(music_object_structure)

    NoteDetector.apply_flat_sharp(music_object_structure)
    NoteDetector.apply_change_duration(music_object_structure)

    current_step.set("Création du XML et du MIDI")
    root.update()
    music_xml_creator = MusicXMLCreator(output_dir='OUT/')
    music_xml_creator.parse(tempo=tempo_mid, music_objects=music_object_structure)
    music_xml_creator.generate_musicxml()
    music_xml_creator.generate_midi()

    current_step.set("Lecture terminée.")
    root.update()

    # for el in music_object_structure:
    # print(el)
    # Display notes and symbols on the score
    
    for note in notes:
        start = note.position.to_start_point()
        # cv.putText(img, text=str(note.pitch.step), org=start, fontFace=cv.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0), thickness=2, lineType=cv.LINE_AA)
        ##    for note in notes:
        ##        start = note.position.to_start_point()
        end = note.position.to_end_point()
        img = cv.rectangle(img, start, end, (255, 0, 0), 1)
        #cv.putText(img, text=str(note.quavers), org=start,
        #           fontFace=cv.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0),
        #           thickness=2, lineType=cv.LINE_AA)

    ##    for ss in symbols_structure:
    ##        cv.putText(img, text = str(ss[3]), org = (int(ss[1]), int(ss[2])), fontFace=cv.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 0, 255), thickness=2, lineType=cv.LINE_AA)

    note_img = img.copy()
    for i in range(0, len(music_object_structure)):
        if type(music_object_structure[i][0]) is NoteType:
            cv.putText(note_img, text = music_object_structure[i][4].str_code, org = (int(music_object_structure[i][1]), int(music_object_structure[i][2])), fontFace=cv.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 0, 255), thickness=2, lineType=cv.LINE_AA)

    dur_img = img.copy()
    for i in range(0, len(music_object_structure)):
        if type(music_object_structure[i][0]) is NoteType:
            cv.putText(dur_img, text = str(music_object_structure[i][4].duration), org = (int(music_object_structure[i][1]), int(music_object_structure[i][2])), fontFace=cv.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 0, 255), thickness=2, lineType=cv.LINE_AA)

    
    
    for symbol in sr.symbols:
        start = symbol.position.to_start_point()
        end = symbol.position.to_end_point()
        img = cv.rectangle(img, start, end, (255, 0, 0), 1)

    cv.imwrite(result_path, img)
    cv.imwrite('OUT/quavers.jpeg', detector.img_quavers)
    cv.imwrite('OUT/duration.jpeg', dur_img)
    cv.imwrite('OUT/notes.jpeg', note_img)

    return result_path
