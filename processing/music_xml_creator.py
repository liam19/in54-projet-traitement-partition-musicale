import music21 as m21

from models.note_type import NoteType
from models.symbol import SymbolType


class Clef:
    def __init__(self, clef, key_signature):
        self.clef = clef
        self.key_signature = key_signature

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, Clef):
            return False

        return self.clef == o.clef and self.key_signature == o.key_signature


class MusicXMLCreator:
    __CLEFS = {
        SymbolType.CLE_SOL: m21.clef.GClef,
        SymbolType.CLE_FA: m21.clef.FClef,
    }

    __RESTS = {
        SymbolType.SILENCE: 'quarter',
        SymbolType.DEMI_SILENCE: 'eighth',
        SymbolType.QUART_SILENCE: '16th',
        SymbolType.SILENCE2: 'half',
        SymbolType.SILENCE4: 'whole',
    }

    __NOTES = {
        NoteType.BLACK: 'quarter',
        NoteType.WHITE: 'half',
        NoteType.WHOLE: 'whole',
    }

    def __init__(self, output_dir='../', filename='out'):
        self.output_dir = output_dir
        self.filename = filename
        self.music_stream = None
        self.music_objects = None

    def parse(self, tempo, music_objects):
        if len(music_objects) == 0:
            raise Exception("`music_objects` is empty")

        self.music_objects = music_objects
        s = m21.stream.Score([m21.tempo.MetronomeMark(number=tempo)])
        part = m21.stream.Part()
        s.append(part)

        i, measure, last_clef = self.__get_header(0, tempo)
        part.append(measure)

        i += 1
        line = music_objects[0][3]
        i_line = 0
        while i < len(self.music_objects):
            obj = self.music_objects[i]

            if obj[3] != line:
                line = obj[3]
                if line == 4:
                    pass
                i_line = i

            if isinstance(obj[0], SymbolType):
                if obj[0] in self.__CLEFS.keys():
                    i, measure2, clef = self.__get_header(i)

                    if clef != last_clef:
                        if len(part) != 0 and self.__is_measure(i_line - 1):
                            part.pop(-1)

                        measure = measure2

                        part = m21.stream.Part()
                        s.append(part)

                        part.append(measure)
                        last_clef = clef

                elif obj[0] in self.__RESTS.keys():
                    i, r = self.__get_rest(i)
                    measure.append(r)
                elif obj[0] == SymbolType.MESURE:
                    measure = m21.stream.Measure()
                    part.append(measure)

            elif self.__is_note(i):
                i, n = self.__get_note(i)
                measure.append(n)

            i += 1

        if len(part) != 0 and self.__is_measure(i - 1):
            part.pop(-1)

        self.music_stream = s
        # self.music_stream.show('musicxml.png')

    def __get_header(self, starting_ind=0, tempo=-1):
        i = starting_ind
        clef = self.__get_clef(self.music_objects[starting_ind][0])
        ks_dict = {
            SymbolType.DIESE: 0,
            SymbolType.BEMOL: 0,
        }

        for obj in self.music_objects[i + 1:]:
            actual = ks_dict.get(obj[0])
            if actual is None:
                break

            ks_dict[obj[0]] += 1
            i += 1

        most_common = max(ks_dict, key=ks_dict.get)
        if tempo == -1:
            m = m21.stream.Measure([clef])
        else:
            m = m21.stream.Measure([m21.tempo.MetronomeMark(number=tempo), clef])

        if ks_dict[most_common] != 0:
            m.keySignature = self.__get_key_signature(most_common, ks_dict[most_common])

        if i + 1 < len(self.music_objects) and self.music_objects[i + 1][0] == SymbolType.STRUCTURE:
            m.timeSignature = m21.meter.TimeSignature('4/4')
            i += 1

        return i, m, Clef(clef, m.keySignature)

    def __get_clef(self, symbol_type: SymbolType):
        return self.__CLEFS[symbol_type]()

    def __get_rest(self, i):
        obj = self.music_objects[i]
        r = m21.note.Rest()
        r.duration = m21.duration.Duration(self.__RESTS[obj[0]])
        return i, r

    def __get_note(self, i, h_thresh=5):
        o_obj = self.music_objects[i]
        line = o_obj[3]
        notes = []
        obj = o_obj

        while i < len(self.music_objects) and self.__is_note(i) and line == obj[3] and \
                abs(obj[1] - o_obj[1]) <= h_thresh:
            n = m21.note.Note(obj[4].str_code)
            n.duration = m21.duration.Duration(self.__NOTES[obj[0]])
            n.duration.quarterLength = obj[4].duration

            notes.append(n)
            i += 1
            obj = self.music_objects[i]

        i -= 1

        if len(notes) == 1:
            n = notes[0]
        else:
            n = m21.chord.Chord(notes)

        return i, n

    @staticmethod
    def __get_key_signature(symbol_type: SymbolType, nb_occurrence):
        if symbol_type == SymbolType.DIESE:
            return m21.key.KeySignature(nb_occurrence)
        else:
            return m21.key.KeySignature(-nb_occurrence)

    def __is_measure(self, i):
        return isinstance(self.music_objects[i][0], SymbolType) and self.music_objects[i][0] == SymbolType.MESURE

    def __is_note(self, i):
        return isinstance(self.music_objects[i][0], NoteType)

    def generate_midi(self):
        if self.music_stream is None:
            raise Exception("Please run MusicXMLCreator.parse before calling this method")

        self.music_stream.write(fmt='midi', fp='{}/{}.midi'.format(self.output_dir, self.filename))

    def generate_musicxml(self):
        if self.music_stream is None:
            raise Exception("Please run MusicXMLCreator.parse before calling this method")

        self.music_stream.write(fmt='musicxml', fp='{}/{}.musicxml'.format(self.output_dir, self.filename))
