import cv2 as cv
import numpy as np
from math import sqrt
from models.rectangle import Rectangle
from models.note import Note
from models.pitch import Pitch
from models.note_type import NoteType
from models.symbol_type import SymbolType
from processing.segmenter import RecursiveProjectionSegmenter
from utils.score_processing_utils import do_staff_removal, count_horizontal_lines, get_v_proj, remove_duplicates, note_to_str


class NoteDetector:
    template_black = cv.imread('resources/templates/notes/quarter.png', 0)
    template_white = cv.imread('resources/templates/notes/half-note-space.png', 0)
    template_white_line = cv.imread('resources/templates/notes/half-note-line.png', 0)
    template_whole = cv.imread('resources/templates/notes/whole-note-space.png', 0)
    template_whole_line = cv.imread('resources/templates/notes/whole-note-line.png', 0)


    def __init__(self, staff_thickness, staff_height, symbols:list, music_staves:list, threshold=0.20):
        self.staff_thickness = staff_thickness
        self.staff_height = staff_height
        print("-----")
        print(staff_height)
        self.threshold = threshold  # 0.62 pour beethov & gonville, emmentaler ; 0.8 pour lily
        self.symbols = symbols
        self.music_staves = music_staves
        self.img_gray = None
        self.img_without_lines = None
        self.img_quavers = None


    def find_notes(self, src_img):
        self.img_gray = cv.cvtColor(src_img, cv.COLOR_BGR2GRAY)

        img_bin = cv.threshold(
            self.img_gray, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]

        self.img_without_lines = do_staff_removal(
            img_bin, self.staff_height)

        self.img_quavers = np.zeros(self.img_without_lines.shape)
        self.img_quavers = np.zeros(src_img.shape)
        self.img_quavers[:, :, 0] = self.img_without_lines

        notes = []

        notes.extend(self.__find_template_match(
            self.template_black, NoteType.BLACK))

        notes.extend(self.__find_template_match(
            self.template_white, NoteType.WHITE))
        notes.extend(self.__find_template_match(
            self.template_white_line, NoteType.WHITE))

        notes.extend(self.__find_template_match(
            self.template_whole, NoteType.WHOLE))
        notes.extend(self.__find_template_match(
            self.template_whole_line, NoteType.WHOLE))

        self.__complete_note_creation(notes)

        return notes

    def __find_template_match(self, template, template_type):
        notes = []

        w, h = template.shape[::-1]  # get the template's height and width

        # locate and extract elements from img_gray according to template
        res = cv.matchTemplate(self.img_gray, template, cv.TM_CCOEFF_NORMED)
        loc = np.where(res >= self.threshold)
        coords = list(zip(*loc[::-1]))
        final_coords = remove_duplicates(
            coords)  # clean up duplicates coords

        if len(final_coords) == 0:
            return notes

        #if template_type == NoteType.BLACK:
            #final_coords = final_coords[1:]  # remove example black note

        # Create notes
        for coord in final_coords:
            rec = Rectangle(coord[0], coord[1], w, h) # build the rectangle

            # compute the octave and the step of the note
            pitch = self.compute_pitch(rec)

            # build the associated note with current informations (type & bounding rectangle)
            note = Note(pitch, 0, template_type, rec)

            # add the new note to the list
            notes.append(note)

        return notes


    def __complete_note_creation(self, notes):
        note_groups = []

        # Group notes
        vertical_tol = 6 * (self.staff_thickness + self.staff_height)
        open_notes = set(notes)

        while len(open_notes) != 0:
            actual_note = open_notes.pop()
            gp = [actual_note]
            to_remove = set()

            for n in open_notes:
                if self.__is_on_same_stem(actual_note, n, vertical_tol=vertical_tol):
                    to_remove.add(n)
                    gp.append(n)

            open_notes -= to_remove
            note_groups.append(gp)

        for ng in note_groups:
            nb_quavers = self.__find_quavers(
                ng, min_flag_height=3 * self.staff_thickness)
            for n in ng:
                n.quavers = nb_quavers
                n.compute_duration()

        # Remove duplicates
        for ng in note_groups:
            if len(ng) == 1:
                pass

            self.__remove_duplicates_on_same_stem(ng, notes)


    def __remove_duplicates_on_same_stem(self, gp, notes, threshold=0.6):
        open_notes = set(gp)

        while len(open_notes) != 0:
            actual_note: Note = open_notes.pop()
            current_area = actual_note.position.area()

            to_remove = set()

            for n in open_notes:
                intersecting_area = n.position.intersecting_area(actual_note.position)
                total_area = n.position.area() + current_area - intersecting_area
                c = intersecting_area / total_area
                if c >= threshold:
                    actual_note.position = Rectangle.from_two_rects(actual_note.position, n.position)
                    current_area = actual_note.position.area()
                    notes.remove(n)
                    to_remove.add(n)

            open_notes -= to_remove



    def compute_pitch(self, rec:Rectangle):
        x, y = rec.get_gravity_center()
        note_pitch = "err"
        for stave in self.music_staves:
            for i in range(0, len(stave.staff_lines_pos)):
                if stave.contains(x, y):
                    note_pitch = (stave.staff_lines_pos[4] - y) / (self.staff_height + 1)
                    note_pitch = note_pitch * 2
                    note_pitch = int(round(note_pitch))
                    break
        
        pitch = Pitch(note_pitch)

        return pitch
        


    def __is_on_same_stem(self, note_1: Note, note_2: Note, horizontal_tol=5, vertical_tol=100, max_gap=5):
        c1 = note_1.position.get_gravity_center()
        c2 = note_2.position.get_gravity_center()

        if abs(c1[0] - c2[0]) > horizontal_tol:
            return False

        if abs(c1[1] - c2[1]) > vertical_tol:
            return False

        box = Rectangle.from_two_rects(note_1.position, note_2.position)
        img = box.crop_img(self.img_without_lines)

        return count_horizontal_lines(img, min_gap=max_gap) == 1


    def __find_quavers(self, notes: list, margin=5, stem_threshold=80,
                       min_flag_gap=2, min_flag_height=3, flag_threshold=40):
        if len(notes) == 0:
            return 0

        rec: Rectangle = notes[0].position
        rec = rec.create_origin()

        true_note_rect: Rectangle = self.__find_note_enclosure(rec, margin)

        if true_note_rect is None:
            return 0

        stem: Rectangle = self.__find_stem(true_note_rect, stem_threshold)
        stem_enlarged = stem.horizontal_enlarge(margin, true_note_rect.w)

        stem_neighbours = np.copy(stem_enlarged.crop_img(self.img_without_lines))

        self.img_quavers[
            stem_enlarged.y_o:stem_enlarged.y_o+stem_enlarged.h,
            stem_enlarged.x_o:stem_enlarged.x_o + stem_enlarged.w,
            2
        ] = stem_neighbours

        # Remove stem from neighbours
        stem_neighbours[:, margin:margin + stem.w] = 0

        for note in notes:
            note_head: Rectangle = note.position.vertical_enlarge(
                margin, stem.h).create_origin()

            start = note_head.to_start_point(
            )[1] - stem_enlarged.to_start_point()[1]
            end = note_head.h + start
            start = start if start > 0 else 0
            end = end if end < stem_enlarged.h else stem_enlarged.h

            stem_neighbours[start:end, :] = 0   # Remove note head

        self.img_quavers[
            stem_enlarged.y_o:stem_enlarged.y_o+stem_enlarged.h,
            stem_enlarged.x_o:stem_enlarged.x_o + stem_enlarged.w,
            1
        ] = stem_neighbours

        nb_quavers = count_horizontal_lines(
            stem_neighbours, min_flag_gap, min_flag_height, flag_threshold)
        return nb_quavers


    def __find_note_enclosure(self, note_head: Rectangle, margin):
        enclosing_rec: Rectangle = note_head.horizontal_enlarge(
            margin, self.img_gray.shape[0])

        enclosing_rec.y = enclosing_rec.y_o = 0
        enclosing_rec.h = self.img_gray.shape[0]

        img_cropped = np.copy(enclosing_rec.crop_img(self.img_without_lines))

        segmenter = RecursiveProjectionSegmenter()
        rects = segmenter.segment(img_cropped)
        rect: Rectangle = note_head.create_origin()
        rect.x_o -= enclosing_rec.x_o

        found = None
        for r in rects:
            if r.contains_point(*rect.get_gravity_center()):
                found = r
                break

        if found is not None:
            start = enclosing_rec.to_start_point()
            found.x_o += start[0]
            found.y_o += start[1]

        return found


    def __find_stem(self, note_rect: Rectangle, threshold=70):
        stem = note_rect.create_origin()
        img = note_rect.crop_img(self.img_without_lines)

        proj = get_v_proj(img)
        m = np.max(proj)
        thresh = m * threshold / 100
        candidates = np.argwhere(proj > thresh)

        if len(candidates) < 2:
            return stem

        stem.x_o += candidates[0][0]
        stem.w = len(candidates)

        return stem

    @staticmethod
    def get_notes_structure(nd, staves):
        structure = []
        for note in nd:
            note.set_str_code(note_to_str(note, staves))
            x, y = note.position.get_gravity_center()
            for i in range(0, len(staves)):
                if staves[i].contains(x, y):
                    structure.append([note.note_type, note.position.get_gravity_center()[0], note.position.get_gravity_center()[1], i, note])

        return structure

    @staticmethod
    def apply_flat_sharp(structure):
        all_lines = True
        for i in range(0, len(structure)):
            if structure[i][0] == SymbolType.DIESE and all_lines:
                n = i + 1
                while structure[n][3] == structure[n - 1][3] and n < len(structure) - 1:
                    if type(structure[n][0]) is NoteType:
                        if structure[n][4].pitch.step == structure[i][4].pitch:
                            structure[n][4].set_sharp()
                    n = n + 1
            elif structure[i][0] == SymbolType.BEMOL and all_lines:
                n = i + 1
                while structure[n][3] == structure[n - 1][3] and n < len(structure) - 1:
                    if type(structure[n][0]) is NoteType:
                        if structure[n][4].pitch.step == structure[i][4].pitch:
                            structure[n][4].set_flat()
                    n = n + 1
            elif type(structure[i][0]) is NoteType and all_lines:
                all_lines = False
            elif structure[i][0] == SymbolType.CLE_SOL or structure[i][0] == SymbolType.CLE_FA:
                all_lines = True
            elif structure[i][0] == SymbolType.DIESE:
                n = i + 1
                while structure[n][0] != SymbolType.MESURE:
                    if type(structure[n][0]) is NoteType:
                        if structure[n][4].pitch.step == structure[i][4].pitch:
                            structure[n][4].set_sharp()
                    n = n + 1
            elif structure[i][0] == SymbolType.BEMOL:
                n = i + 1
                while structure[n][0] != SymbolType.MESURE:
                    if type(structure[n][0]) is NoteType:
                        if structure[n][4].pitch.step == structure[i][4].pitch:
                            structure[n][4].set_flat()
                    n = n + 1
            elif structure[i][0] == SymbolType.BECARRE:
                n = i + 1
                while structure[n][0] != SymbolType.MESURE:
                    if type(structure[n][0]) is NoteType:
                        if structure[n][4].pitch.step == structure[i][4].pitch:
                            structure[n][4].set_natural()
                    n = n + 1
                    

    @staticmethod
    def apply_change_duration(structure):
        for i in range(0, len(structure)):
            if structure[i][0] == SymbolType.POINT:
                note = NoteDetector.__find_nearest(structure[i][4].position, structure, i)
                if note is not None:
                    # structure[i - 1][4].apply_point()
                    note[4].apply_point()

    @staticmethod
    def __find_nearest(r: Rectangle, structure, i, depth=10):
        dists = []
        x2, y2 = r.get_gravity_center()

        for s in structure[i - depth:i] + structure[i:i + depth]:
            if isinstance(s[0], NoteType):
                x1, y1 = s[4].position.get_gravity_center()
                d = NoteDetector.__dist(x1, x2, y1, y2)
                d = (d - 100 / d) if (y1 > y2 and x1 < x2) else d
                dists.append((d, s))

        if len(dists) == 0:
            return None

        return min(dists, key=lambda x: x[0])[1]

    @staticmethod
    def __dist(x1, x2, y1, y2):
        d = sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
        return d
