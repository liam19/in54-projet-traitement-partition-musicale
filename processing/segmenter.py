from models.rectangle import Rectangle
from utils.score_processing_utils import get_h_proj, get_v_proj
import numpy as np


class RecursiveProjectionSegmenter:
    """
    Describes a segmentation algorithm using recursive cuts along x and y axes by computing horizontal and vertical
    projections.
    """

    def __init__(self, max_iter=10, threshold=2):
        """Creates an instance of `RecursiveProjectionSegmenter`

        Args:
            max_iter([int], default=10): defines the maximum number of recursive iterations
            threshold([int], default=2): defines how far an element need to be to consider it as a separate element
        """
        self.max_iter = max_iter
        self.threshold = threshold

    @staticmethod
    def _add_rect(x, w, img, direction, rectangles, nb_rect, x_o=0, y_o=0):
        if direction == 0:
            rect = Rectangle(0, x, img.shape[1], w, x_o=x_o, y_o=y_o)
        else:
            rect = Rectangle(x, 0, w, img.shape[0], x_o=x_o, y_o=y_o)

        rectangles[nb_rect] = rect

        return nb_rect + 1

    def _segment_on_axis(self, img, direction, bounding_rect):
        assert len(img) != 0

        cropped = bounding_rect.crop_img(img)

        if direction == 0:
            proj = get_h_proj(cropped)
        else:
            proj = get_v_proj(cropped)

        non_zeros = np.argwhere(proj > 0)

        if len(non_zeros) == 0:  # White image
            return []

        rectangles = np.array([None] * len(non_zeros))
        nb_rect = 0

        last = non_zeros[0]
        x = last
        for nz in non_zeros:
            if nz - last > 1 + self.threshold:
                nb_rect = self._add_rect(x, last - x + 1, cropped, direction, rectangles, nb_rect,
                                         bounding_rect.x_o, bounding_rect.y_o)
                x = nz

            last = nz

        nb_rect = self._add_rect(x, non_zeros[-1] - x + 1, cropped, direction, rectangles, nb_rect,
                                 bounding_rect.x_o, bounding_rect.y_o)
        return rectangles[0:nb_rect]

    def _segment_on_both_axis(self, img, bounding_rect=None):
        if bounding_rect is None:
            bounding_rect = Rectangle(0, 0, img.shape[1], img.shape[0])

        bounding_rect = bounding_rect.create_origin()

        # Horizontal cut
        rectangles = self._segment_on_axis(img, 0, bounding_rect)
        new_rectangles = []

        # Vertical cut
        for r in rectangles:
            new_rectangles.extend(
                self._segment_on_axis(img, 1, r.create_origin()))

        return new_rectangles

    def segment(self, img):
        """Segments the image `img` with the parameters of `self`

        Args:
            img([type]): image to process

        Returns:
            [list(Rectangle)]: a list of `Rectangle` containing extracted features
        """
        candidates = self._segment_on_both_axis(img)
        final_rectangles = []
        i = 0
        while len(candidates) and i < self.max_iter:
            new_candidates = []
            for c in candidates:
                rectangles = self._segment_on_both_axis(img, c)
                if len(rectangles) == 0:
                    continue
                elif len(rectangles) == 1:
                    final_rectangles.append(rectangles[0])
                else:
                    new_candidates.extend(rectangles)

            candidates = new_candidates
            i += 1

        final_rectangles.extend(candidates)
        return final_rectangles
