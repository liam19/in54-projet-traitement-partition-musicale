import cv2 as cv
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from utils.score_processing_utils import do_staff_removal, remove_duplicates
from models.rectangle import Rectangle
from models.symbol_type import SymbolType
from models.symbol import Symbol

class SymbolRecognition:
    def __init__(self, src_img, staff_height, music_staves):
        self.symbols = []
        self.music_staves = music_staves
        self.staff_height = staff_height
        img_gray = cv.cvtColor(src_img, cv.COLOR_BGR2GRAY)
        img_bin = cv.threshold(img_gray, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)[1]
        self.img_nolines = cv.bitwise_not(do_staff_removal(img_bin, staff_height))

        self.path = "resources/templates/" + str(self.find_style(src_img)) + "/"
        print(self.path)
        
        self.find_all_symbols(src_img)
        # cv.imwrite("sansligne.png", self.img_nolines)
        
    def find_style(self, img):
        df = pd.read_csv("resources/templates/verif_name.csv")
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        names = df["name"].to_numpy()
        types = df["type"].to_numpy()

        style = -1

        for i in range(0, names.size):
            if style == -1:
                template = cv.imread("resources/templates/type_verif/" + names[i] + ".png", 0)
                res = cv.matchTemplate(gray , template, cv.TM_CCOEFF_NORMED)
                loc = np.where(res >= 0.9)
                if len(loc[0]) != 0:
                    style = types[i]
        return style
        
    def find_all_symbols(self, src_img):
        img_gray = cv.cvtColor(src_img, cv.COLOR_BGR2GRAY)
        df = pd.read_csv("resources/templates/templates_name.csv")
        names = df["t_name"].to_numpy()
        treshs = df["t_tresh"].to_numpy()
        lines = df["t_line"].to_numpy()
        types = df["t_type"].to_numpy()
        for i in range(0, treshs.size):
            # print(self.path + "template_" + str(names[i]) + ".png")
            if lines[i] == 0:
                exec('self.find_symbol(src_img, self.img_nolines, cv.imread(self.path + "template_" + str(names[i]) + ".png", 0), treshs[i], SymbolType.' + types[i] + ')')
            else:
                exec('self.find_symbol(src_img, img_gray, cv.imread(self.path + "template_" + str(names[i]) + ".png", 0), treshs[i], SymbolType.' + types[i] + ')')
            
    def find_symbol(self, src_img, img_gray, template, threshold, symbol_type):
        print(symbol_type)
        w, h = template.shape[::-1]
        res = cv.matchTemplate(img_gray, template, cv.TM_CCOEFF_NORMED)
        loc = np.where(res >= threshold)
        # print(symbol_type)

        coords = list(zip(*loc[::-1]))
        final_coords = remove_duplicates(coords, 20)  # clean up duplicates coords

        # Create notes
        for coord in final_coords:
            # build the rectangle
            rec = Rectangle(coord[0], coord[1], w, h)

            # build the associated note with current informations (type & bounding rectangle)
            symbol = Symbol(symbol_type, rec)
            symbol.set_pitch(SymbolRecognition.compute_pitch(symbol, self.music_staves, self.staff_height))
            

            # add the new note to the list
            self.symbols.append(symbol)

        # for pt in zip(*loc[::-1]):
        #     cv.rectangle(src_img, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)

        # cv.imwrite("res.png", src_img)
    @staticmethod
    def get_symbols_structure(sr, staves):
        structure = []
        for symbol in sr.symbols:
            x, y = symbol.position.get_gravity_center()
            for i in range(0, len(staves)):
                if staves[i].contains(x, y):
                    structure.append([symbol.symbol_type, x, y, i, symbol])
                    if symbol.symbol_type == SymbolType.CLE_SOL or symbol.symbol_type == SymbolType.CLE_FA:
                        staves[i].set_key(symbol.symbol_type)
##        structure_res = []
##        for i in range(0, len(staves)):
##            temp = list(filter(lambda struc: struc[3] == i, structure))
##            temp.sort(key = lambda struc : struc[1])
##            structure_res += temp
        structure = SymbolRecognition.delete_fake_silence(structure)
        
        return structure

    @staticmethod
    def delete_fake_silence(structure, threshold = 10):
        new_struc = structure
        for i in range(0, len(structure)):
            if structure[i][0] == SymbolType.QUART_SILENCE:
                if structure[i-1][0] == SymbolType.DEMI_SILENCE and structure[i-1][1] - threshold < structure[i][1] < structure[i-1][1] + threshold:
                    del new_struc[i-1]
                if structure[i+1][0] == SymbolType.DEMI_SILENCE and structure[i+1][1] - threshold < structure[i][1] < structure[i+1][1] + threshold:
                    del new_struc[i+1]
        return new_struc

    @staticmethod
    def compute_pitch(symbol, staves, staff_height):
        pitch = 0
        x, y = symbol.position.get_gravity_center()
        for stave in staves:
            for i in range(0, len(stave.staff_lines_pos)):
                if stave.contains(x, y):
                    pitch = (stave.staff_lines_pos[4] - y) / (staff_height+1)
                    pitch = pitch * 2
                    pitch = int(round(pitch))
                    break

        if symbol.symbol_type == SymbolType.BEMOL:
            return pitch % 7 - 1
        else:
            return pitch % 7
            

    
if __name__ == "__main__":
##    img = cv.imread("../../dataset/deep_scores_dense_extended/images_png/lg-4610032-aug-beethoven--page-2.png")
##    dim = (2707, 3828)
##    img_r = cv.resize(, dim, interpolation = cv.INTER_AREA)
    
    sr = SymbolRecognition(cv.imread("../../dataset/deep_scores_dense_extended/images_png/lg-3948783-aug-lilyjazz--page-1.png"), cv.cvtColor(cv.imread("../../result.png"), cv.COLOR_BGR2GRAY))
    
